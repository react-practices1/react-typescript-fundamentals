type HeaderProps = {
    children: string
}

export const Heading = (props: HeaderProps) => {
    return <h2>{props.children}</h2>
}